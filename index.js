require('dotenv').config();

const express = require('express');

const { PrismaClient } = require('@prisma/client')
const fs = require("fs");

const prisma = new PrismaClient()

const app = express();

app.use(express.json());

app.get('/api/card/:km', async (req, res) => {
    if (!req.headers.authorization || req.headers.authorization !== 'Bearer ' + process.env.AUTHORIZATION) {
        res.send({
            status: 401,
            message: 'Unauthorized, 未授权的请求'
        });
        return;
    }
    const card = await prisma.card.findUnique({
        where: {
            km: req.params.km
        }
    });
    if (!card) {
        res.send({
            status: 404,
            message: 'Not Found, 卡密不存在'
        });
        return;
    }
    const Delete = JSON.parse(fs.readFileSync('./delete.json').toString());
    card.deleted = Delete[card.km] || false;
    res.send({
        status: 200,
        message: 'OK',
        data: card
    });
});

app.delete('/api/card/:km/:id', async (req, res) => {
    if (!req.headers.authorization || req.headers.authorization !== 'Bearer ' + process.env.AUTHORIZATION) {
        res.send({
            status: 401,
            message: 'Unauthorized, 未授权的请求'
        });
        return;
    }
    const card = await prisma.card.findUnique({
        where: {
            km: req.params.km
        }
    });
    if (!card) {
        res.send({
            status: 404,
            message: 'Not Found, 卡密不存在'
        });
        return;
    }
    const Delete = JSON.parse(fs.readFileSync('./delete.json').toString());
    if(!Delete[card.km]) {
        Delete[card.km] = true;
        Delete[card.km + '.id'] = req.params.id;
        fs.writeFileSync('./delete.json',JSON.stringify(Delete));
    }
    res.send({
        status: 200,
        message: 'OK'
    });
});

app.listen(process.env.PORT || 11982, () => {
    console.log('Card API Server Started at ' + (process.env.PORT || 11982));
});
